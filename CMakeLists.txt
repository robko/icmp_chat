CMAKE_MINIMUM_REQUIRED(VERSION 2.8.3)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++14 -Wall -Wextra -pedantic")

add_executable(ping_example ping.cpp)
target_link_libraries(ping_example boost_system pthread)

add_executable(icmp_chat main.cpp)
target_link_libraries(icmp_chat boost_system)