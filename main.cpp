#include <boost/asio.hpp>

class c_network_io final {
	public :
		c_network_io();
		void send_with_checksum(const boost::asio::ip::address_v4 &dst_address, uint16_t checksum);
		
	private:
		boost::asio::io_service m_ioservice;
		boost::asio::ip::icmp::socket m_socket;
		uint16_t m_sequence_number;
		std::array<uint8_t, 58> m_payload;
};

c_network_io::c_network_io()
:
	m_ioservice(),
	m_socket(m_ioservice),
	m_sequence_number(0)
{
	std::fill(m_payload.begin(), m_payload.end(), 0);
}

/*
template <typename Iterator>
void compute_checksum(icmp_header& header,
    Iterator body_begin, Iterator body_end)
{
  unsigned int sum = (header.type() << 8) + header.code()
    + header.identifier() + header.sequence_number();

  Iterator body_iter = body_begin;
  while (body_iter != body_end)
  {
    sum += (static_cast<unsigned char>(*body_iter++) << 8);
    if (body_iter != body_end)
      sum += static_cast<unsigned char>(*body_iter++);
  }

  sum = (sum >> 16) + (sum & 0xFFFF);
  sum += (sum >> 16);
  header.checksum(static_cast<unsigned short>(~sum));
}
 */
void c_network_io::send_with_checksum(const boost::asio::ip::address_v4 &dst_address, uint16_t checksum_arg) {
	uint32_t checksum = ~checksum_arg;
	
	const uint16_t rand = 0x1234 + (checksum % 2);
	checksum = checksum - rand;
	checksum = ((checksum / 2) << 16) + rand;
}

// TODO rm this function
bool checksum_test(uint16_t checksum_arg) {
	uint32_t checksum = ~checksum_arg;
	const uint16_t rand = 0x1234 + (checksum % 2);
	checksum = checksum - rand;
	checksum = ((checksum / 2) << 16) + rand;
	
	uint32_t sum = checksum;
	sum = (sum >> 16) + (sum & 0xFFFF);
	sum += (sum >> 16);
	unsigned short ret_sum = static_cast<unsigned short>(~sum);
	return checksum_arg == ret_sum;
}

int main() {
	//  test
	for (uint16_t i = 0; i < 100; i++) {
		std::cout << i << std::endl;
		if (!checksum_test(i)) break;
	}
	return 0;
}
